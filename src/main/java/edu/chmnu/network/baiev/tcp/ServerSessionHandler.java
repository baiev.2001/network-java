package edu.chmnu.network.baiev.tcp;

import java.net.Socket;

@FunctionalInterface
public interface ServerSessionHandler {
    void handle(Socket socket) throws Exception;
}
