package edu.chmnu.network.baiev.tcp.caesar.model;

public enum Result {
    TWO_ROOTS,
    ONE_ROOT,
    NO_ROOTS
}
