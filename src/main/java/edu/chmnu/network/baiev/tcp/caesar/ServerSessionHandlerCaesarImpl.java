package edu.chmnu.network.baiev.tcp.caesar;

import edu.chmnu.network.baiev.tcp.ServerSessionHandler;
import edu.chmnu.network.baiev.tcp.caesar.model.CaesarRequest;
import edu.chmnu.network.baiev.tcp.caesar.model.CaesarResponse;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerSessionHandlerCaesarImpl implements ServerSessionHandler {
    @Override
    public void handle(Socket socket) throws Exception {
        try (ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
             ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream())) {
            while (!socket.isClosed()) {
                CaesarRequest request = (CaesarRequest) in.readObject();
                System.out.println(">" + request);
                CaesarResponse response = processRequest(request);
                out.writeObject(response);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ServerSessionHandlerCaesarImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private CaesarResponse processRequest(CaesarRequest request) {
        char[] text = request.getText().toCharArray();
        int symbolShift = request.getShift();
        for (int i = 0; i < text.length; ++i) {
            char symbol = text[i];
            if (symbol == ' ') {
//                Do nothing
            } else if (symbol >= 97 && symbol < 123) {
                int originalAlphabetPosition = symbol - 'a';
                int newAlphabetPosition = (originalAlphabetPosition + symbolShift) % 26;
                text[i] = (char) ('a' + newAlphabetPosition);
            } else if (symbol >= 65 && symbol < 91) {
                int originalAlphabetPosition = symbol - 'A';
                int newAlphabetPosition = (originalAlphabetPosition + symbolShift) % 26;
                text[i] = (char) ('A' + newAlphabetPosition);
            }
        }
        return new CaesarResponse(symbolShift, new String(text));
    }

}
