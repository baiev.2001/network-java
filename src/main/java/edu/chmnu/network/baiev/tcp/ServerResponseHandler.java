package edu.chmnu.network.baiev.tcp;

@FunctionalInterface
public interface ServerResponseHandler {
    void handle(Object response) throws Exception;

}
