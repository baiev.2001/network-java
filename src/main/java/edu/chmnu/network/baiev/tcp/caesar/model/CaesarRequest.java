package edu.chmnu.network.baiev.tcp.caesar.model;

import java.io.Serializable;

public class CaesarRequest implements Serializable {
    private int shift;

    private String text;

    public CaesarRequest(int shift, String text) {
        this.shift = shift;
        this.text = text;
    }

    public int getShift() {
        return shift;
    }

    public void setShift(int shift) {
        this.shift = shift;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "CaesarRequest{" +
                "shift=" + shift +
                ", text='" + text + '\'' +
                '}';
    }
}
