package edu.chmnu.network.baiev.tcp.caesar;

import edu.chmnu.network.baiev.tcp.ServerResponseHandler;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class TCPRequestExecutorImpl implements Runnable {
    private final Object data;
    private final Socket socket;
    private final ServerResponseHandler handler;

    public TCPRequestExecutorImpl(String host, int port, Object data, ServerResponseHandler handler) throws IOException {
        this.data = data;
        this.handler = handler;
        this.socket = new Socket(host, port);
        this.socket.setSoTimeout(1000);
    }

    public TCPRequestExecutorImpl(Socket socket, Object data, ServerResponseHandler handler) throws IOException {
        this.data = data;
        this.handler = handler;
        this.socket = socket;
    }


    @Override
    public void run() {
        try (ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
             ObjectInputStream in = new ObjectInputStream(socket.getInputStream())) {

            out.writeObject(data);
            Object response = in.readObject();
            System.out.printf("[%s:%d] Received: %s\n",
                    socket.getInetAddress().toString(), socket.getPort(),
                    response.toString());
            handler.handle(response);
        } catch (SocketTimeoutException ex) {
            System.out.println("No ops on the socket: " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("IOException occurred");
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            System.out.println("Can't found class of object to read");
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
