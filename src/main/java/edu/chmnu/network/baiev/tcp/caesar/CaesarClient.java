package edu.chmnu.network.baiev.tcp.caesar;

import edu.chmnu.network.baiev.tcp.ServerResponseHandler;
import edu.chmnu.network.baiev.tcp.caesar.model.CaesarRequest;
import edu.chmnu.network.baiev.tcp.caesar.model.CaesarResponse;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CaesarClient {
    public static void main(String[] args) throws InterruptedException, IOException {
        String textToSend = "ZZZZ";


//        double[][] testData = {
//                {-1, 2, -3},
//                {4, 5, 1},
//                {1, 4, 4}
//        };

        String host = "127.0.0.1";
        int port = 5558;
        ExecutorService executor = Executors.newFixedThreadPool(10);
//        for () {
        executor.submit(new TCPRequestExecutorImpl(host, port, new CaesarRequest(2, textToSend), new CaesarResponseHandler()));
//        }

        Thread.sleep(2000);
        executor.shutdown();
    }

    public static class CaesarResponseHandler implements ServerResponseHandler {

        @Override
        public void handle(Object response) {
            if (!(response instanceof CaesarResponse)) {
                throw new RuntimeException("Expected response to be type 'Response'");
            }
            CaesarResponse convertedResponse = (CaesarResponse) response;
            System.out.println("Encoded: " + convertedResponse.getEncodedText());
        }
    }
}