package edu.chmnu.network.baiev.tcp.caesar.model;

import java.io.Serializable;

public class CaesarResponse implements Serializable {
    private int shift;

    private String encodedText;

    public CaesarResponse() {
    }

    public CaesarResponse(int shift, String encodedText) {
        this.shift = shift;
        this.encodedText = encodedText;
    }

    public int getShift() {
        return shift;
    }

    public void setShift(int shift) {
        this.shift = shift;
    }

    public String getEncodedText() {
        return encodedText;
    }

    public void setEncodedText(String encodedText) {
        this.encodedText = encodedText;
    }

    @Override
    public String toString() {
        return "CaesarResponse{" +
                "shift=" + shift +
                ", encodedText='" + encodedText + '\'' +
                '}';
    }
}
